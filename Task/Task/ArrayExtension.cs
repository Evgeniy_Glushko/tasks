﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class ArrayExtension
    {
        private List<int> _list;
        private int _sum;

        public ArrayExtension()
        {
            Random random = new Random();
            _list = new List<int>();
            for (int i = 0; i < 5; i++)
            {
                _list.Add(random.Next(0, 500));
            }
        }

        public int this[int index]
        {
            get { return _list[index]; }
            set { _list[index] = value; }
        }

        public int Remove(int index)
        {
            _sum = _list.Sum();
            _list.Remove(_list[index]);
            return _sum - _list.Sum();
        }
        public IEnumerator<int> GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}
