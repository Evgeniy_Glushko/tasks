﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
    class Duplicate
    {
        private Dictionary<char, int> _dictionary;

        public Duplicate()
        {
            _dictionary = new Dictionary<char, int>();
        } 
        public IEnumerable<char> Search(char[] str)
        {
            foreach (var elem in str)
            {
                if (_dictionary.ContainsKey(elem))
                    _dictionary[elem] += 1;
                else
                {
                    _dictionary[elem] = 1;
                }
            }
            IEnumerable<char> list = _dictionary.Where(x => x.Value > 1).Select(x => x.Key);
            return list;
        }
    }
}
