﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task
{
   
    class MyList
    { 
        public Node Head;
        private int _count;
        public class Node
        {
            public int Data;
            public Node Next;
        }


        private void Add()
        {
            Node newNode = new Node();
            Console.WriteLine("Write a number:");
            newNode.Data = int.Parse(Console.ReadLine());
            if (Head != null)
            {
                Node helpNode = Head;
                while (helpNode.Next != null) 
                helpNode = helpNode.Next;
                helpNode.Next = newNode;
            }
            else
            {
                Head = newNode;
            }
        }
       
        public void printList()
        {
            Node helpNode = Head;
            while (helpNode != null)
            {
                Console.WriteLine(helpNode.Data);
                helpNode = helpNode.Next;
            }
        }
       
      public MyList(int count)
        {
            _count = count;
            for (int i = 0; i < count; i++) 
                Add();
        }

        public int GetAverageElement()
        {
            Node helpNode = Head;
            for (int i = 0; i < _count/2; i++)
            {
              helpNode = helpNode.Next;
            }
            return helpNode.Data;
        }

    }
}

